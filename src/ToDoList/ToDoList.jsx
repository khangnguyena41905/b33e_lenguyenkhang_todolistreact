import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Dropdown } from "../Components/Dropdown";
import { Container } from "../Components/Container";
import { ToDoListDarkTheme } from "../themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../themes/ToDoListPrimaryTheme";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "../Components/Heading";
import { Label, TextField } from "../Components/TextField";
import { Button } from "../Components/Button";
import { Table, Th, Thead, Tr } from "../Components/Table";
import { Hr } from "../Components/Hr";
import { connect } from "react-redux";
import {
  add_task,
  change_theme,
  chang_task_status,
  delete_task,
  edit_task,
  update_task,
} from "../redux/action/toDoListAction";
import { arrThemes } from "../themes/ThemeManagers";

class ToDoList extends Component {
  state = {
    taskName: "",
  };
  renderTaskToDo = (taskList) => {
    return taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.handleTaskEdit(task);
                }}
              >
                <i className="fa fa-edit" />
              </Button>
              <Button
                onClick={() => {
                  this.props.handleChangeStatus(task.id);
                }}
              >
                <i className="fa fa-check" />
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDeleteTask(task.id);
                }}
              >
                <i className="fa fa-trash" />
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskComplete = (taskList) => {
    return taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.handleChangeStatus(task.id);
                }}
              >
                <i class="fa fa-sync"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleDeleteTask(task.id);
                }}
              >
                <i className="fa fa-trash" />
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return arrThemes.map((theme) => {
      return (
        <option key={theme.id} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  render() {
    let { themeToDoList, taskList, taskEdit } = this.props;
    return (
      <ThemeProvider theme={themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let selectorVal = e.target.value;
              this.props.handleChangeTheme(selectorVal);
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3 className="mt-3">To do list</Heading3>
          <TextField
            value={this.state.taskName}
            onChange={(e) => {
              let taskName = e.target.value;
              this.setState({
                taskName,
              });
            }}
            label={"Task name"}
          />
          <Button
            onClick={() => {
              // lấy thông tin từ state
              let { taskName } = this.state;
              // tạo object task mới
              let newTask = {
                id: Date.now(),
                taskName,
                done: false,
              };
              // gửi object lên store
              this.props.handleAddTask(newTask);
            }}
            className="ml-2"
          >
            {" "}
            <i className="fa fa-plus" /> Add tasks
          </Button>
          <Button
            onClick={() => {
              this.props.handleUpdateTask(this.state.taskName);
            }}
            className="ml-2"
          >
            {" "}
            <i className="fa fa-upload" /> Update task
          </Button>
          <Hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo(taskList)}</Thead>
          </Table>
          <Heading3>Task complete</Heading3>
          <Table>
            <Thead>{this.renderTaskComplete(taskList)}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({ taskName: this.props.taskEdit.taskName });
    }
  }
}

let mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};
let mapDispathToProps = (dispath) => {
  return {
    handleAddTask: (newTask) => {
      dispath(add_task(newTask));
    },
    handleChangeTheme: (themeId) => {
      dispath(change_theme(themeId));
    },
    handleChangeStatus: (taskId) => {
      dispath(chang_task_status(taskId));
    },
    handleDeleteTask: (taskId) => {
      dispath(delete_task(taskId));
    },
    handleTaskEdit: (task) => {
      dispath(edit_task(task));
    },
    handleUpdateTask: (taskName) => {
      dispath(update_task(taskName));
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(ToDoList);
