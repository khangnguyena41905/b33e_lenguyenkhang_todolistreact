import { combineReducers } from "redux";
import ToDoListReducer from "./ToDoListReducer";

export let rootReducer = combineReducers({
  ToDoListReducer,
});
