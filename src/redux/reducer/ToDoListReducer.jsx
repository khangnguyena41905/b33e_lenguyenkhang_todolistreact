import { arrThemes } from "../../themes/ThemeManagers";
import { ToDoListDarkTheme } from "../../themes/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../themes/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../themes/ToDoListPrimaryTheme";
import {
  ADD_TASK,
  CHANGE_TASK_STATUS,
  CHANGE_THEME,
  DELETE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../contant/toDoListContant";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task 1", taskName: "task 1", done: true },
    { id: "task 2", taskName: "task 2", done: false },
    { id: "task 3", taskName: "task 3", done: true },
    { id: "task 4", taskName: "task 4", done: false },
  ],
  taskEdit: { id: "task 1", taskName: "task 1", done: true },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK: {
      let newTaskList = [...state.taskList];
      //   kiểm tra rỗng
      if (payload.taskName.trim() === "") {
        alert("Task name is required!");
        return { ...state };
      }
      // kiểm tra tồn tại
      let index = newTaskList.findIndex((task) => {
        return task.taskName === payload.taskName.trim();
      });
      if (index !== -1) {
        alert("Task already exists");
        return { ...state };
      }
      // khi đã kiểm tra xong
      let newTask = { ...payload, taskName: payload.taskName.trim() };
      newTaskList.push(newTask);
      state.taskList = newTaskList;
      console.log("newTaskList: ", newTaskList);
      return { ...state };
    }
    case CHANGE_THEME: {
      let arrTheme = [...arrThemes];
      let index = arrTheme.findIndex((e) => {
        return e.id == payload;
      });
      return { ...state, themeToDoList: arrTheme[index].theme };
    }
    case CHANGE_TASK_STATUS: {
      let taskLists = [...state.taskList];
      let index = taskLists.findIndex((task) => {
        return task.id == payload;
      });
      taskLists[index].done = !taskLists[index].done;
      return { ...state, taskList: taskLists };
    }
    case DELETE_TASK: {
      let taskLists = [...state.taskList];
      let newTaksLists = taskLists.filter((task) => {
        return task.id !== payload;
      });
      return { ...state, taskList: newTaksLists };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: payload };
    }
    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, taskName: payload };
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id == state.taskEdit.id;
      });
      taskListUpdate[index] = state.taskEdit;
      return { ...state, taskList: taskListUpdate };
    }
    default:
      return state;
  }
};
