export const ADD_TASK = "ADD_TASK";
export const CHANGE_THEME = "CHANGE_THEME";
export const CHANGE_TASK_STATUS = "CHANGE_STATUS";
export const DELETE_TASK = "DELETE_TASK";
export const EDIT_TASK = "EDIT_TASK";
export const UPDATE_TASK = "UPDATE_TASK";
