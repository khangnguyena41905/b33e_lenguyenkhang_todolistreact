import {
  ADD_TASK,
  CHANGE_TASK_STATUS,
  CHANGE_THEME,
  DELETE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../contant/toDoListContant";

export const add_task = (payload) => ({
  type: ADD_TASK,
  payload,
});
export const change_theme = (payload) => ({
  type: CHANGE_THEME,
  payload,
});
export const chang_task_status = (payload) => ({
  type: CHANGE_TASK_STATUS,
  payload,
});
export const delete_task = (payload) => ({
  type: DELETE_TASK,
  payload,
});
export const edit_task = (payload) => ({
  type: EDIT_TASK,
  payload,
});
export const update_task = (payload) => ({
  type: UPDATE_TASK,
  payload,
});
