import styled from "styled-components";

export const Hr = styled.hr`
  margin-top: 1rem;
  margin-bottom: 1rem;
  border: 0;
  border-top: 1px solid ${(props) => props.theme.color};
`;
